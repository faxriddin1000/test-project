import { Component, OnDestroy, OnInit } from '@angular/core';
import { MapService } from '../service/map.service';



@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit, OnDestroy {
  constructor(private mapService: MapService) { }


  ngOnInit(): void {
    // load map
    this.mapService.initMap();

  }


  ngOnDestroy(): void {
    this.mapService.unsubscriper();
  }

}
