import {Component, OnDestroy, OnInit} from '@angular/core';
import { List } from '../service/model/list';
import { HttpService  } from '../service/http.service';
import {Subscription} from "rxjs";

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],

})
export class ListComponent implements OnInit, OnDestroy {
  public client: List[];
  private  unsubscripe: Subscription;
  constructor(private http: HttpService) {
  }
  ngOnInit(): void {
    this.unsubscripe = this.http.getClientData('./api/client.json')
        .subscribe((res: List[])=>{
         this.client = res;
     }, (err) =>{
       console.log("error" + err);
    });
  }

  ngOnDestroy(): void {
     this.unsubscripe.unsubscribe();
  }


}
