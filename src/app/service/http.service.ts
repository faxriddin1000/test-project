import { List } from './model/list';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Marker } from './model/marker';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private http: HttpClient) { }


  /*
     return List[]
  */

  public getClientData(url: string){
    return this.http.get<List[]>(url);
  }


  /*
     return Marker[]
  */

  public getMarkerData(url: string){
    return this.http.get<Marker[]>(url)
  }


}
