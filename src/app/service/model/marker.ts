import { MarkerCoord } from "./markercoord";

export interface Marker {
    id: number;
    type: string;
    price: number;
    coords: MarkerCoord;
    client_id: number
  }
  