export interface MarkerCoord{
    lat: number;
    long: number;
}