import { Marker } from './model/marker';
import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import * as L from 'leaflet';
import  'leaflet.markercluster';
import { Subscription } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class MapService {

  private  unsubscripe: Subscription;
  private map: L.Map;
  private markers  = L.markerClusterGroup({ maxClusterRadius: 60,showCoverageOnHover: false,spiderfyOnMaxZoom: true });
  constructor(private http: HttpService) {}


  public loadMarker(){
    this.unsubscripe = this.http.getMarkerData('./api/nerolag.json').subscribe((next: Marker[]) =>{
      next.forEach((element: Marker) => {
        this.addMarker(element);
       });

    }, (err) =>{
      console.log("error" + err);
    })
  }


  

  // load map
  public initMap(): void{
    this.map = L.map('map',{
       center: [43.238949, 76.889709],
       zoom: 12,
    })
  
    const tiles = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution:  '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
        minZoom: 3,
      });
  
      tiles.addTo(this.map);
     
      // method call on add points 
      this.loadMarker();
  }



  public unsubscriper(){
    this.unsubscripe.unsubscribe();
  }




// method  add point
private addMarker(data: Marker): void{
  const popupOption = this.getPopup(data);
  const marker = L.marker(
    new L.LatLng(data.coords.lat, data.coords.long)
   )
   marker.bindPopup(popupOption.template, popupOption.option);
   this.markers.addLayer(marker);

   this.map.addLayer(this.markers);
}



 // method add popup for point 
  private getPopup(data: Marker){
    const DELIVERY = 'delivery';
    const APPLICATIONTYPE = data.type === DELIVERY ? 'Доставка' : 'Забор';
  
  const popupTemplate: string =`
        <div class="popup">
          <span class="popup-title">ID заявки:  </span>
          <span>${data.id}</span>
        </div>
        <div class="popup">
            <span class="popup-title">Тип заявки:  </span>
            <span >${APPLICATIONTYPE}</span>
          </div>
        <div class="popup">
            <span class="popup-title">Цена заявки:   </span>
            <span>${data.price}</span>
        </div>
        <div class="popup">
            <span class="popup-title">Адрес заявки:   </span>
            <span> Lat: ${data.coords.lat},   Long:  ${data.coords.long}</span>
        </div>
        <div class="popup">
          <span class="popup-title">ID клиента:</span>
          <span>${data.client_id}</span>
        </div>

      `
    const popupoption = {
      'width': 'auto',
      'className' : 'popupCustom'
      }

    return {option: popupoption, template: popupTemplate};

  }

}
