
Angular - The modern web developer's platform.
angular-logo
Angular is a development platform for building mobile and desktop web applications
using Typescript/JavaScript and other languages.
www.angular.io
Contributing Guidelines · Submit an Issue · Blog

CI status   Angular on npm   Discord conversation

Documentation
Get started with Angular, learn the fundamentals and explore advanced topics on our documentation website.

Getting Started
Architecture
Components and Templates
Forms
API
Advanced
Angular Elements
Server Side Rendering
Schematics
Lazy Loading
Development Setup
Prerequisites
Install Node.js which includes Node Package Manager
Setting Up a Project
Install the Angular CLI globally:

npm install nodejs
Install Node.js

npm install -g @angular/cli
Create workspace:

ng new [PROJECT NAME]
Run the application:

cd [PROJECT NAME]
ng serve
Angular is cross-platform, fast, scalable, has incredible tooling, and is loved by millions.

Quickstart
Get started in 5 minutes.

Ecosystem
angular ecosystem logos

Angular Command Line (CLI)
Angular Material
Changelog
Learn about the latest improvements.

Upgrading
Check out our upgrade guide to find out the best way to upgrade your project.

Contributing
Contributing Guidelines
Read through our contributing guidelines to learn about our submission process, coding rules and more.

Want to Help?
Want to report a bug, contribute some code, or improve documentation? Excellent! Read up on our guidelines for contributing and then check out one of our issues labeled as help wanted or good first issue.

Code of Conduct
Help us keep Angular open and inclusive. Please read and follow our Code of Conduct.

Community
Join the conversation and help the community.

Twitter
Discord
Gitter
YouTube
StackOverflow
Find a Local Meetup
Love Angular badge

Love Angular? Give our repo a star ⭐ ⬆️.
